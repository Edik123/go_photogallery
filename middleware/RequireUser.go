package middleware

import (
	"strings"
	"net/http"
    "gallery/models"
    "gallery/middleware/context"
)

type User struct {
    models.UserService
}

func (mw *User) Apply(next http.Handler) http.HandlerFunc {
    return mw.ApplyFn(next.ServeHTTP)
}

func (mw *User) ApplyFn(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		path := r.URL.Path
		// If the user is requesting a static asset or image
		// we will not need to lookup the current user
		if strings.HasPrefix(path, "/assets/") || strings.HasPrefix(path, "/images/") {
		   next(w, r)
		   return
		}
		
		// Check if a user is logged in
		cookie, err := r.Cookie("remember_token")
		if err != nil {
		   next(w, r)
		   return
		}
		user, err := mw.UserService.ByRemember(cookie.Value)
		if err != nil {
		   next(w, r)
		   return
		}
		ctx := r.Context()  // Get the context from our request
		// Create a new context from the existing one that has our user stored in it with the private user key
		ctx = context.WithUser(ctx, user)
		// Create a new request from the existing one with our context attached to it and assign it back to `r`
		r = r.WithContext(ctx)
		next(w, r)
	})
}

type RequireUser struct {
    User
}

func (mw *RequireUser) Apply(next http.Handler) http.HandlerFunc {
    return mw.ApplyFn(next.ServeHTTP)
}

func (mw *RequireUser) ApplyFn(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		user := context.User(r.Context())
		if user == nil {
		   http.Redirect(w, r, "/login", http.StatusFound)
		   return
		}
		next(w, r)
	})
}