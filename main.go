package main
import (
    "fmt"
    "flag"
    "net/http"
    "gallery/models"
    "gallery/middleware"
    "gallery/controllers"
    "gallery/helpers/rand"
    "github.com/gorilla/mux"
    "github.com/gorilla/csrf"
)

func pageNotFound(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")
	fmt.Fprint(w, "<h1>Sorry, page not found!</h1>")
}

func main() {
	fprod:= flag.Bool("prod", false, "Provide this flag in production."+
		" This ensures that a .config file is provided before the application starts.")
	flag.Parse()
	conf := LoadConfig(*fprod)
    dbConf := conf.Database
	services , err := models.NewServices(
		models.WithGorm(dbConf.Dialect(), dbConf.ConnectionInfo()),
        models.WithLogMode(!conf.IsProd()),
        models.WithUser(conf.Pepper, conf.HMACKey),
		models.WithGallery(),
		models.WithImage(),
    )
	errorHandler(err)
	defer services.Close()
	services.AutoMigrate()

    r := mux.NewRouter()
    
    static := controllers.NewStatic()
    users := controllers.NewUsers(services.User)
    galleries := controllers.NewGalleries(services.Gallery, services.Image, r) 

	b, err := rand.Bytes(32)
	errorHandler(err)
    csrfMw := csrf.Protect(b, csrf.Secure(conf.IsProd()))
    userMw := middleware.User{
	    UserService: services.User,
	}
    requireUserMw := middleware.RequireUser{
	    User: userMw,
	}

    r.NotFoundHandler = http.HandlerFunc(pageNotFound)
	r.Handle("/", static.Home).Methods("GET")
	r.Handle("/contact", static.Contact).Methods("GET")
	r.HandleFunc("/signup", users.New).Methods("GET")
	r.HandleFunc("/signup", users.Create).Methods("POST")
	r.Handle("/login", users.LoginView).Methods("GET")
    r.HandleFunc("/login", users.Login).Methods("POST")
    r.Handle("/logout", requireUserMw.ApplyFn(users.Logout)).Methods("POST")
    r.Handle("/forgot", users.ForgotPwView).Methods("GET")
	r.HandleFunc("/forgot", users.InitiateReset).Methods("POST")
	r.HandleFunc("/reset", users.ResetPw).Methods("GET")
	r.HandleFunc("/reset", users.CompleteReset).Methods("POST")

    // Assets
	assetHandler := http.FileServer(http.Dir("./assets/"))
	r.PathPrefix("/assets/").Handler(http.StripPrefix("/assets/", assetHandler))

    // Gallery routes
    r.Handle("/galleries", requireUserMw.ApplyFn(galleries.Index)).Methods("GET").Name(controllers.IndexGalleries)
    r.Handle("/galleries/new", requireUserMw.Apply(galleries.New)).Methods("GET")
    r.HandleFunc("/galleries", requireUserMw.ApplyFn(galleries.Create)).Methods("POST")
    r.HandleFunc("/galleries/{id:[0-9]+}", galleries.Show).Methods("GET").Name(controllers.ShowGallery)
    r.HandleFunc("/galleries/{id:[0-9]+}/edit", requireUserMw.ApplyFn(galleries.Edit)).Methods("GET").Name(controllers.EditGallery)
    r.HandleFunc("/galleries/{id:[0-9]+}/update", requireUserMw.ApplyFn(galleries.Update)).Methods("POST")
    r.HandleFunc("/galleries/{id:[0-9]+}/delete", requireUserMw.ApplyFn(galleries.Delete)).Methods("POST")

    // Image routes
    imageHandler := http.FileServer(http.Dir("./images/"))
    r.PathPrefix("/images/").Handler(http.StripPrefix("/images/", imageHandler))
    r.HandleFunc("/galleries/{id:[0-9]+}/images", requireUserMw.ApplyFn(galleries.ImageUpload)).Methods("POST")
    r.HandleFunc("/galleries/{id:[0-9]+}/images/{filename}/delete", requireUserMw.ApplyFn(galleries.ImageDelete)).Methods("POST")

	fmt.Println("Server is listening...")
    http.ListenAndServe("localhost:3000", csrfMw(userMw.Apply(r)))
}

// A helper function that panics on any error
func errorHandler(err error) {
    if err != nil {
       panic(err)
    }
}