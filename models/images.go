package models

import(
    "io"
    "os"
    "fmt"
    "net/url"
    "path/filepath"
)

type Image struct {
    GalleryID uint
    Filename string
}

type ImageService interface {
    Create(galleryID uint, r io.Reader, filename string) error
    ByGalleryID(galleryID uint) ([]Image, error)
    Delete(i *Image) error
}

type imageService struct{}

func NewImageService() ImageService {
    return &imageService{}
}

func (is *imageService) Create(galleryID uint, r io.Reader, filename string) error {
	galleryPath, err := is.mkImagePath(galleryID)
	if err != nil {
	   return err
	}

    dst, err := os.Create(filepath.Join(galleryPath, filename))
	if err != nil {
	   return err
	}
	defer dst.Close()

	// Copy uploaded file data to the destination file
	_, err = io.Copy(dst, r)
	if err != nil {
	   return err
	}
	return nil
}

func (is *imageService) Delete(i *Image) error {
    return os.Remove(i.RelativePath())
}

func (is *imageService) ByGalleryID(galleryID uint) ([]Image, error) {
	path := is.imagePath(galleryID)
	strings, err := filepath.Glob(filepath.Join(path, "*"))
	if err != nil {
	   return nil, err
	}
	img := make([]Image, len(strings))
	for i, imgStr  := range strings {
        img[i] = Image{
        	GalleryID: galleryID,
		    Filename: filepath.Base(imgStr),
		}
    }
	return img, nil
}

func (is *imageService) mkImagePath(galleryID uint) (string, error) {
	galleryPath := is.imagePath(galleryID) // filepath.Join will return a path images/galleries/123
	err := os.MkdirAll(galleryPath, 0755)  // Create our directory using 0755 permissions
	if err != nil {
       return "", err
    }
    return galleryPath, nil
}

func (is *imageService) imagePath(galleryID uint) string {
    return filepath.Join("images", "galleries", fmt.Sprintf("%v", galleryID))
}

func (i *Image) Path() string {
    temp := url.URL{
	    Path: "/" + i.RelativePath(),
	}
	return temp.String()
}

func (i *Image) RelativePath() string {
	galleryID := fmt.Sprintf("%v", i.GalleryID)   // Convert the gallery ID to a string
	return filepath.ToSlash(filepath.Join("images", "galleries", galleryID, i.Filename))
}